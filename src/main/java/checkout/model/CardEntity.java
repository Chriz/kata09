package checkout.model;

public class CardEntity  {
	private String item;
	private float price;

	public CardEntity(CardEntity entity) {
		super();
		this.item = new String(entity.getItem());
		this.price = entity.getPrice();
	}
	
	public CardEntity(String item) {
		super();
		this.item = item;
		this.price = 0;
	}

	public CardEntity(String item, float price) {
		super();
		this.item = item;
		this.price = price;
	}

	public float getPrice() {
		return price;
	}

	public String getItem() {
		return item;
	}

	public boolean compare(CardEntity arg0) {

		if (item.equals(arg0.getItem())) {
			return true;
		}
		return false;
	}
}