package checkout.model;

import java.util.HashMap;
import java.util.Map;

public class Card extends HashMap<CardEntity, Integer> {

	private static final long serialVersionUID = -514101895737919468L;

	public Card() {
		super();
	}

	public Card(Card card) {
		super();
		for (Map.Entry<CardEntity, Integer> entity : card.entrySet()) {
			put(new CardEntity(entity.getKey()), new Integer(entity.getValue()));
		}
	}

	public Integer get(CardEntity entity) {
		for (Map.Entry<CardEntity, Integer> tempEntry : super.entrySet()) {
			if (entity.compare(tempEntry.getKey())) {
				return tempEntry.getValue();
			}
		}
		return null;
	}

	public void add(CardEntity entity) {
		Integer amount = get(entity);
		if (amount == null) {
			amount = 0;
		}
		put(entity, amount + 1);
	}

	private void put(CardEntity entity, int amount) {
		for (CardEntity tempEntry : super.keySet()) {
			if (entity.compare(tempEntry)) {
				super.put(tempEntry, amount);
				return;
			}
		}
		super.put(entity, amount);
	}

	private void removeTotal(CardEntity entity) {
		CardEntity toRemove = null;
		for (CardEntity tempEntry : super.keySet()) {
			if (entity.compare(tempEntry)) {
				toRemove = tempEntry;
			}
		}
		if (toRemove != null) {
			super.remove(toRemove);
		}
	}

	public void add(String name) {
		CardEntity entity = new CardEntity(name);
		add(entity);
	}

	public boolean contains(CardEntity entity) {
		if (get(entity) != null) {
			return true;
		}
		return false;
	}

	public boolean contains(String name) {
		CardEntity entity = new CardEntity(name);
		return contains(entity);
	}

	public void remove(String name) {
		CardEntity entity = new CardEntity(name);
		remove(entity);
	}

	public void remove(CardEntity entity) {
		if (contains(entity)) {
			int amount = get(entity);
			if (amount == 1) {
				removeTotal(entity);
			} else {
				put(entity, amount - 1);
			}
		}
	}

	public void remove(String name, int amount) {
		CardEntity entity = new CardEntity(name);
		remove(entity, amount);
	}

	public void remove(CardEntity entity, int amount) {
		for (int i = 0; i < amount; i++) {
			remove(entity);
		}
	}

	public Integer get(String name) {
		CardEntity entity = new CardEntity(name);
		return get(entity);
	}

	public float getTotal() {
		float total = 0;
		for (Map.Entry<CardEntity, Integer> entity : entrySet()) {
			total += entity.getKey().getPrice() * entity.getValue();
		}
		return total;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("\n");
		for (Map.Entry<CardEntity, Integer> entity : super.entrySet()) {
			stringBuilder.append(entity.getValue() + "       " + entity.getKey().getItem() + "    "
					+ entity.getValue() * entity.getKey().getPrice() + "\n");
		}
		return stringBuilder.toString();
	}
	
}
