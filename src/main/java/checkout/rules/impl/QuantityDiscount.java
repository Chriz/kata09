package checkout.rules.impl;

import checkout.model.Card;
import checkout.model.CardEntity;
import checkout.rules.AbstractRule;

public class QuantityDiscount extends AbstractRule {

	private String item;
	private CardEntity entity;

	public QuantityDiscount(int count, String item, float price) {
		super(count);
		this.item = item;
		this.entity = new CardEntity("QuantityDiscount for " + item + " singlePrice: " + price, price);
	}

	@Override
	protected void use(Card card) {
		if (card.contains(item) && card.get(item) >= count) {
			while (card.contains(item) && card.get(item) >= 1) {
				card.remove(item);
				card.add(entity);
			}
		}
	}
}
