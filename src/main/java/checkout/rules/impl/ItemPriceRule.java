package checkout.rules.impl;

import checkout.model.Card;
import checkout.model.CardEntity;
import checkout.rules.AbstractRule;

public class ItemPriceRule extends AbstractRule {

	private String item;
	private CardEntity entity;

	public ItemPriceRule(int count, String item, float price) {
		super(count);
		this.item = item;
		this.entity = new CardEntity(count + " " + item + " for ", price);
	}

	@Override
	protected void use(Card card) {
		while (card.contains(item) && card.get(item) >= count) {
			card.remove(item, count);
			card.add(entity);
		}
	}

}
