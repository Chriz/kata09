package checkout.rules.impl;

import checkout.model.Card;
import checkout.model.CardEntity;
import checkout.rules.AbstractRule;

public class Discount extends AbstractRule {

	private int discount;

	public Discount(int discount) {
		super(0);
		this.discount = discount;
		
	}

	@Override
	protected void use(Card card) {
		if (!card.isEmpty()) {
			float total = card.getTotal();
			float discountAmount = -(total / 100 * discount);
			card.add(new CardEntity("Discount of " + discount + "%  ", discountAmount));
		}

	}

}
