package checkout.rules;

import java.util.Collections;
import java.util.List;

import checkout.model.Card;

public class RuleProvider {
	private List<AbstractRule> ruleList;

	public void add(AbstractRule rule) {
		ruleList.add(rule);
		sortRuleList();
	}

	public void add(List<AbstractRule> ruleList) {
		this.ruleList = ruleList;
		sortRuleList();
	}

	public float getPrice(Card card) {
		ruleList.get(0).handleCard(card);
		return card.getTotal();
	}

	private void sortRuleList() {
		Collections.sort(ruleList);
		for (int i = 0; i < ruleList.size() - 1; i++) {
			AbstractRule rule = ruleList.get(i);
			rule.setNext(ruleList.get(i + 1));
		}
	}
}
