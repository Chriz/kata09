package checkout.rules;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import checkout.rules.impl.Discount;
import checkout.rules.impl.ItemPriceRule;
import checkout.rules.impl.QuantityDiscount;

@Configuration
public class RulesConfig {
	
	@Bean
	public List<AbstractRule> ruleListSimple() {
		ArrayList<AbstractRule> list =  new ArrayList<>();
		list.add(new ItemPriceRule(1, "A", 50));
		list.add(new ItemPriceRule(3, "A", 130));
		list.add(new ItemPriceRule(1, "B", 30));
		list.add(new ItemPriceRule(2, "B", 45));
		list.add(new ItemPriceRule(1, "C", 20));
		list.add(new ItemPriceRule(1, "D", 15));
		list.add(new ItemPriceRule(2, "D", 15));
		return list;
	}
	
	@Bean
	public List<AbstractRule> ruleListQuantityDiscount() {
		ArrayList<AbstractRule> list =  new ArrayList<>();
		list.add(new QuantityDiscount(3, "A", 30));
		list.add(new ItemPriceRule(1, "A", 50));
		list.add(new ItemPriceRule(3, "A", 130));
		return list;
	}
	
	@Bean
	public List<AbstractRule> ruleListDiscount() {
		ArrayList<AbstractRule> list =  new ArrayList<>();
		list.add(new Discount(10));
		list.add(new ItemPriceRule(1, "A", 50));
		list.add(new ItemPriceRule(1, "B", 30));
		list.add(new ItemPriceRule(1, "C", 20));
		list.add(new ItemPriceRule(1, "D", 15));
		list.add(new ItemPriceRule(3, "A", 130));
		return list;
	}
	
}
