package checkout.rules;

import checkout.model.Card;

public abstract class AbstractRule implements Comparable<AbstractRule> {

	protected int count;
	private AbstractRule next = null;

	public AbstractRule(int count) {
		super();
		this.count = count;
	}

	public void handleCard(Card card) {
		use(card);
		if (next != null) {
			next.handleCard(card);
		}
	}

	protected abstract void use(Card card);

	@Override
	public int compareTo(AbstractRule o) {
		int oCount = o.getCount();
		if (count == oCount) {
			return 0;
		} else if (count < oCount) {
			return 1;
		} else {
			return -1;
		}
	}

	public int getCount() {
		return count;
	}

	public void setNext(AbstractRule next) {
		this.next = next;
	}
}
