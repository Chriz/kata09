package checkout;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import checkout.model.Card;
import checkout.rules.AbstractRule;
import checkout.rules.RuleProvider;

@Component
public class Checkout {

	private static Logger LOG  = Logger.getLogger(Checkout.class);
	private Card card;
	private RuleProvider ruleProvider;
	
	public Checkout(List<AbstractRule> ruleListSimple){
		ruleProvider = new RuleProvider();
		ruleProvider.add(ruleListSimple);
		card = new Card();
	}
	
	public void scan(String item) {
		card.add(item);
	}
	
	public void pay() {
		float total = ruleProvider.getPrice(card);
		LOG.info("\nCount    Item       Price" + card.toString() + "------------------------------\nTotal: " + total);
		card.clear();
	}
	
	public float total() {
		return ruleProvider.getPrice(new Card(card));
	}
	
}
