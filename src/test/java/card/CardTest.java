package card;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import checkout.model.Card;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CardTest extends TestCase {

	@Test
	public void testBasic() {
		Card card = new Card();
		card.add("Test");
		assertEquals(1, card.size());
		assertEquals(true, card.contains("Test"));
		card.remove("Test");
		assertEquals(0, card.size());
	}
	@Test
	public void testMultiple() {
		Card card = new Card();
		card.add("A");
		card.add("A");
		card.add("A");
		card.add("A");
		card.add("A");
		assertEquals(new Integer(5), card.get("A"));
	}
}
