package checkout;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import checkout.Checkout;
import checkout.rules.AbstractRule;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DiscountTest extends AbstractCheckoutTest {
	@Autowired
	private List<AbstractRule> ruleListDiscount;

	@Before
	public void setUp() {
		checkout = new Checkout(ruleListDiscount);
	}

	@Test
	public void test() {
		checkout.scan("B");
		checkout.scan("A");
		checkout.scan("B");
		checkout.scan("B");
		checkout.scan("D");
		checkout.scan("A");
		checkout.scan("A");
		checkout.scan("B");
		checkout.scan("C");
		checkout.scan("B");
		
	}

}
