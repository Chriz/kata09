package checkout;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import checkout.Checkout;
import checkout.rules.AbstractRule;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CheckoutTest extends AbstractCheckoutTest {

	@Autowired
	private List<AbstractRule> ruleListSimple;

	@Before
	public void setUp() {
		checkout = new Checkout(ruleListSimple);
	}

	@Test
	public void checkTwoForOne() {
		checkout.scan("D");
		checkout.scan("D");
		assertEquals(15f, checkout.total());
	}

	@Test
	public void checkSingle() {
		checkout.scan("A");
		assertEquals(50f, checkout.total());
		checkout.scan("B");
		assertEquals(80f, checkout.total());
		checkout.scan("C");
		assertEquals(100f, checkout.total());
		checkout.scan("D");
		assertEquals(115f, checkout.total());

	}

	@Test
	public void checkMultiple() {
		checkout.scan("A");
		checkout.scan("A");
		checkout.scan("A");
		assertEquals(130f, checkout.total());
		checkout.scan("B");
		checkout.scan("B");
		assertEquals(175f, checkout.total());
	}

	@Test
	public void checkMix() {
		checkout.scan("B");
		checkout.scan("A");
		checkout.scan("B");
		checkout.scan("B");
		checkout.scan("D");
		checkout.scan("A");
		checkout.scan("A");
		checkout.scan("B");
		checkout.scan("C");
		checkout.scan("B");
		assertEquals(285f, checkout.total());
	}
}
