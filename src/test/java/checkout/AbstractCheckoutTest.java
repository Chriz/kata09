package checkout;

import org.junit.After;

import checkout.Checkout;
import junit.framework.TestCase;

public class AbstractCheckoutTest extends TestCase {

	protected Checkout checkout;

	@After
	public void cleanUp() {
		checkout.pay();
	}

}
