package checkout;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import checkout.Checkout;
import checkout.rules.AbstractRule;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QuantityDiscountTest extends AbstractCheckoutTest {
	@Autowired
	private List<AbstractRule> ruleListQuantityDiscount;

	@Before
	public void setUp() {
		checkout = new Checkout(ruleListQuantityDiscount);
	}

	@Test
	public void checkQuantityDiscount() {
		checkout.scan("A");
		assertEquals(50f, checkout.total());
		checkout.scan("A");
		assertEquals(100f, checkout.total());
		checkout.scan("A");
		assertEquals(90f, checkout.total());
	}
}
